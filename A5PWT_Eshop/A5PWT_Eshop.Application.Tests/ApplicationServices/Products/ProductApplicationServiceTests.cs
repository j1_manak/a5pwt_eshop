﻿using A5PWT_Eshop.Application.ApplicationServices.Products;
using A5PWT_Eshop.Application.Configuration;
using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Products;
using A5PWT_Eshop.Infrastructure.Data;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;

namespace A5PWT_Eshop.Application.Tests.ApplicationServices.Products
{
    public class ProductApplicationServiceTests : IDisposable
    {
        private readonly IServiceScope _scope;
        private readonly IProductApplicationService _productApplicationService;
        private IServiceProvider _serviceProvider => _scope.ServiceProvider;

        public ProductApplicationServiceTests()
        {
            _scope = SetupDI().CreateScope();
            _productApplicationService = _serviceProvider.GetService<IProductApplicationService>();
        }

        [Fact]
        public void GetIndexViewModelTest()
        {
            //Arrange
            var dataContext = _serviceProvider.GetService<DataContext>();
            dataContext.Products.AddRange(
                new Product{ Id = 1, Name ="Produkt 1", Price = 455 },
                new Product { Id = 2, Name = "Produkt 2", Price = 45 }
                );
            dataContext.SaveChanges();

            //Act
            var indexViewModel = _productApplicationService.GetIndexViewModel();

            //Assert
            Assert.NotNull(indexViewModel);
            Assert.True(indexViewModel.Products.Any());

        }

        [Fact]
        public void InsertTest()
        {
            //Arrange
            var image = _scope.ServiceProvider.GetService<IFormFile>();
            var productViewModel = new ProductViewModel
            {
                Name = "Produkt 8",
                Price = 10,
                ImageUrl = "http",
                Image = image
            };
            //Act
            var product = _productApplicationService.Insert(productViewModel);

            //Assert
            Assert.NotNull(product);
            Assert.True(product.Id > 0);
            Assert.Equal(productViewModel.Name, product.Name);
            Assert.Equal(productViewModel.Price, product.Price);
        }

        private IServiceProvider SetupDI()
        {
            var services = new ServiceCollection();
            var environment = new HostingEnvironment
            {
                WebRootPath = @"C:\Users\student\Documents\Maňák\a5pwt_eshop\A5PWT_Eshop\A5PWT_Eshop.Application.Tests\images\output",
                ContentRootPath = @"C:\Users\student\Documents\Maňák\a5pwt_eshop\A5PWT_Eshop\A5PWT_Eshop.Application.Tests\images\output"
            };

            var builder = new ContainerBuilder();
            builder.RegisterModule<ApplicationDependencyModule>();
            ServiceConfiguration.Load(services, environment);
            MockIFormFile(services);
            builder.Populate(services);
            var container = builder.Build();
            return new AutofacServiceProvider(container);
        }

        private void MockIFormFile(ServiceCollection services)
        {
            var fileMock = new Mock<IFormFile>();
            var fileName = "input.jpg";
            var fileInfo = new FileInfo($@"C:\Users\student\Documents\Maňák\a5pwt_eshop\A5PWT_Eshop\A5PWT_Eshop.Application.Tests\images\input\{fileName}");
            var data = new byte[fileInfo.Length];

            using(var fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }

            using (var ms = new MemoryStream())
            {
                using (StreamWriter sw = new StreamWriter(ms))
                {
                    sw.Write(data);
                    sw.Flush();
                    ms.Position = 0;
                    fileMock.Setup(x => x.OpenReadStream()).Returns(ms);
                    fileMock.Setup(x => x.FileName).Returns(fileName);
                    fileMock.Setup(x => x.Length).Returns(ms.Length);
                    fileMock.Setup(x => x.ContentType).Returns("image/jpg");
                }
            }

            services.AddSingleton(fileMock.Object);
        }

        public void Dispose() => _scope.Dispose();
    }
}
