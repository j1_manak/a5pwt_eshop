﻿using A5PWT_Eshop.Domain.Entities.Orders;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Data.Configurations.Orders
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders", "Web");
            builder.HasKey(e => e.Id);
            builder.HasMany(e => e.OrderItems).WithOne(e => e.Order).IsRequired().HasForeignKey(e => e.OrderId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.BillingAddress).WithMany().IsRequired(false).HasForeignKey(e => e.BillingAddressId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.ShippingAddress).WithMany().IsRequired(false).HasForeignKey(e => e.ShippingAddressId)
                .OnDelete(DeleteBehavior.Cascade);

        }
    }
}
