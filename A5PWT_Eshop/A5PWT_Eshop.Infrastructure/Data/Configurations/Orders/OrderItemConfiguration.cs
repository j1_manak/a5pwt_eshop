﻿using A5PWT_Eshop.Domain.Entities.Orders;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Data.Configurations.Orders
{
    public class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.ToTable("OrderItems", "Web");
            builder.HasKey(e => e.Id);
            builder.HasOne(e => e.Product).WithMany().IsRequired().HasForeignKey(e => e.ProductId).OnDelete(DeleteBehavior.Cascade);

        }
    }
}
