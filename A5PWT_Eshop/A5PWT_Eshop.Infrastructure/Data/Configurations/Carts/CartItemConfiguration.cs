﻿using A5PWT_Eshop.Domain.Entities.Carts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Data.Configurations.Carts
{
    public class CartItemConfiguration : IEntityTypeConfiguration<CartItem>
    {
        public void Configure(EntityTypeBuilder<CartItem> builder)
        {
            builder.ToTable("CartItems", "Web");
            builder.HasKey(e => e.CartId);
            builder.HasOne(e => e.Product).WithMany().IsRequired(true).HasForeignKey(e => e.ProductId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
