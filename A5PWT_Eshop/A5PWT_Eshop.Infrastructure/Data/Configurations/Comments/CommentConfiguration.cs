﻿using A5PWT_Eshop.Domain.Entities.Comments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Data.Configurations.Comments
{
    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.ToTable("Comments", "Web");
            builder.HasKey(x => x.Id);
            //builder.HasMany(x => x.Replies).WithOne(x => x.ParentComment).HasForeignKey(x => x.CommentId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
