﻿using A5PWT_Eshop.Domain.Entities.Comments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Data.Configurations.Comments
{
    public class CommentReplyConfiguration : IEntityTypeConfiguration<CommentReply>
    {
        public void Configure(EntityTypeBuilder<CommentReply> builder)
        {
            builder.ToTable("CommentReplies", "Web");
            builder.HasKey(x => x.Id);
        }
    }
}
