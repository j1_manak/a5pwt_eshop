﻿using A5PWT_Eshop.Domain.Entities;
using A5PWT_Eshop.Domain.Entities.Carts;
using A5PWT_Eshop.Domain.Entities.Comments;
using A5PWT_Eshop.Domain.Entities.Orders;
using A5PWT_Eshop.Domain.Entities.Products;
using A5PWT_Eshop.Infrastructure.Identity.Roles;
using A5PWT_Eshop.Infrastructure.Identity.Users;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Data
{
    public class DataContext : IdentityDbContext<User, Role, int>
    {
        public DataContext(DbContextOptions options) : base(options)
        {
            
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<CommentReply> CommentReplies { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach(var entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.Relational().TableName = entity.Relational().TableName.Replace("AspNet", string.Empty);
            }
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();

            var changedEntities = ChangeTracker.Entries().Where(e => e.Entity is Entity && (e.State == EntityState.Modified || e.State == EntityState.Added
                                                                   || e.State == EntityState.Deleted));

            foreach (var item in changedEntities)
            {
                var entity = item.Entity as Entity;
                if(item.State == EntityState.Added)
                {
                    entity.DateCreated = DateTime.Now;
                }
                entity.DateUpdated = DateTime.Now;
            }

            ChangeTracker.AutoDetectChangesEnabled = false;
            var result = base.SaveChanges();
            ChangeTracker.AutoDetectChangesEnabled = true;
            return result;
        }
    }
}
