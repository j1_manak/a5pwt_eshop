﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Security;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Data
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseSqlServer($"Server=databaze.fai.utb.cz;Database=A17133_A5PWT;User ID=A17133;Password=A17133A17133;persist security info = True;" +
                $"multipleactiveresultsets=True");
            return new DataContext(builder.Options);
        }
    }
}
