﻿using A5PWT_Eshop.Domain.Entities.Carts;
using A5PWT_Eshop.Domain.Entities.Products;
using A5PWT_Eshop.Domain.Repositories.Carts;
using A5PWT_Eshop.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Repositories.Carts
{
    public class CartRepository : ICartRepository
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<Cart> _dbSetCarts;
        public CartRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSetCarts = _dataContext.Set<Cart>();
        }

        public CartItem AddToCart(int productID, int amount, string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            var item = cart.CartItems.FirstOrDefault(x => x.ProductId == productID);
            if (item != null)
            {
                item.Amount++;
                _dataContext.SaveChanges();
                return item;
            }
            else
            {
                CartItem cartItem = new CartItem
                {
                    CartId = cart.Id,
                    Amount = amount,
                    ProductId = productID
                };
                cart.CartItems.Add(cartItem);
                _dataContext.SaveChanges(); 
                return cartItem;
            }
        }

        public IList<CartItem> GetCartItems(string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            return cart.CartItems;
        }

        public void RemoveFromCart(int productID, string userTrackingCode)
        {
            var cart = GetCurrentCart(userTrackingCode);
            var cartItem = cart.CartItems.FirstOrDefault(x => x.ProductId == productID);
            if(cartItem == null)
            {
                return;
            }
            cart.CartItems.Remove(cartItem);
            _dataContext.Entry(cartItem).State = EntityState.Deleted;
            _dataContext.SaveChanges();
        }

        public Cart GetCurrentCart(string userTrackingCode)
        {
            var cart = _dbSetCarts
                .Include(x => x.CartItems)
                .ThenInclude(cartItem => cartItem.Product)
                .FirstOrDefault(x => x.UserTrackingCode == userTrackingCode);
            if (cart == null)
            {
                cart = new Cart { UserTrackingCode = userTrackingCode, CartItems = new List<CartItem>() };
                _dbSetCarts.Add(cart);
                _dataContext.SaveChanges();
            }
            return cart;
        }
    }
}
