﻿using A5PWT_Eshop.Domain.Entities.Orders;
using A5PWT_Eshop.Domain.Repositories.Carts;
using A5PWT_Eshop.Domain.Repositories.Orders;
using A5PWT_Eshop.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Repositories.Orders
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ICartRepository _cartRepository;
        private readonly DataContext _dataContext;
        private readonly DbSet<Order> _dbSet;

        public OrderRepository(ICartRepository cartRepository, DataContext dataContext)
        {
            _cartRepository = cartRepository;
            _dataContext = dataContext;
            _dbSet = dataContext.Set<Order>();
        }

        public void CreateOrder(int userId, string userTrackingCode)
        {
            var cart = _cartRepository.GetCurrentCart(userTrackingCode);
            var order = new Order
            {
                UserId = userId,
                // ppřidat jednotlivé adresy a kontaktní informace
                OrderItems = new List<OrderItem>(),
                OrderNumber = userTrackingCode.Take(8).ToString(),
            };

            foreach (var item in cart.CartItems)
            {
                order.OrderItems.Add(new OrderItem
                {
                    ProductId = item.ProductId,
                    Amount = item.Amount,
                    Price = item.Product.Price
                });
            }
            _dbSet.Add(order);
            _dataContext.SaveChanges();
            //vyprázdnit košík
            // v cart repository udělat novou metodu, která z tama vymaže všechny produkty
        }

        public IList<Order> GetOrders(int userID)
        {
            return _dbSet.Where(x => x.UserId == userID).Include(x => x.OrderItems).ToList();
        }
    }
}
