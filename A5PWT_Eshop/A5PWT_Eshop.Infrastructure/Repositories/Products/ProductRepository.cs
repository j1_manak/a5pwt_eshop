﻿using A5PWT_Eshop.Domain.Entities.Products;
using A5PWT_Eshop.Domain.Repositories.Products;
using A5PWT_Eshop.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Repositories.Products
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<Product> _dbSet;

        public ProductRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<Product>();
        }
        public Product Add(Product entity)
        {
            _dbSet.Add(entity);
            _dataContext.SaveChanges();
            return entity;
        }

        public Product Get(Func<Product, bool> predicate)
        {
            return _dbSet.FirstOrDefault(predicate);
        }

        public IList<Product> GetAll()
        {
            return _dbSet.Where(x => !x.IsDeleted).OrderByDescending(x => x.Id).
                //ThenBy(x => x.Price).
                ToList();
        }

        public IList<Product> GetAll(Func<Product, bool> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public Product Remove(Product entity)
        {
            if (!IsAttached(entity))
            {
                _dbSet.Attach(entity);
            }
            entity.IsDeleted = true;
            _dataContext.SaveChanges();
            return entity;
        }

        public Product Update(Product entity)
        {
            if(!IsAttached(entity))
            {
                _dbSet.Attach(entity);
            }
            _dataContext.Entry(entity).State = EntityState.Modified;
            _dataContext.SaveChanges();
            return entity;
        }

        private bool IsAttached(Product entity)
        {
            return _dbSet.Local.Any(e => e == entity);
        }
    }
}
