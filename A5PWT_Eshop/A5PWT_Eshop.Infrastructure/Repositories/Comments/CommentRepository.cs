﻿using A5PWT_Eshop.Domain.Entities.Comments;
using A5PWT_Eshop.Domain.Repositories.Comments;
using A5PWT_Eshop.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Repositories.Comments
{
    public class CommentRepository : ICommentRepository
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<Comment> _dbSet;

        public CommentRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = dataContext.Set<Comment>();
        }

        public Comment Add(Comment entity)
        {
            _dbSet.Add(entity);
            _dataContext.SaveChanges();
            return entity;
        }

        public Comment Delete(Comment entity)
        {
            entity.IsDeleted = true;
            entity.DateUpdated = DateTime.Now;
            _dataContext.Entry(entity).State = EntityState.Modified;
            _dataContext.SaveChanges();
            return entity;
        }

        public IList<Comment> GetAllComments()
        {
            return _dbSet.Where(x => !x.IsDeleted).AsNoTracking().ToList();
        }

        public IList<Comment> GetApprovedComments(int productId)
        {
            return _dbSet.Where(x => !x.IsDeleted && x.ProductId == productId && x.IsApproved).OrderByDescending(x => x.DateCreated).ToList();
        }

        public Comment GetComment(int commentId)
        {
            return _dbSet.Where(x => x.Id == commentId).AsNoTracking().ToList().First(); 
        }

        public Comment Update(Comment entity)
        {
            /*if (!IsAttached(entity))
            {
                _dbSet.Attach(entity);
            }*/
            _dataContext.Entry(entity).State = EntityState.Modified;
            //_dbSet.Update(entity);
            //entity.IsApproved = true;
            _dataContext.SaveChanges();
            return entity;
        }

        private bool IsAttached(Comment entity)
        {
            return _dbSet.Local.Any(e => e == entity);
        }
    }
}
