﻿using A5PWT_Eshop.Domain.Entities.Comments;
using A5PWT_Eshop.Domain.Repositories.Comments;
using A5PWT_Eshop.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace A5PWT_Eshop.Infrastructure.Repositories.Comments
{
    public class CommentReplyRepository : ICommentReplyRepository
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<CommentReply> _dbSet;

        public CommentReplyRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<CommentReply>();
        }

        public CommentReply Add(CommentReply entity)
        {
            _dbSet.Add(entity);
            _dataContext.SaveChanges();
            return entity;
        }

        public CommentReply Delete(CommentReply entity)
        {
            entity.IsDeleted = true;
            entity.DateUpdated = DateTime.Now;
            _dataContext.Entry(entity).State = EntityState.Modified;
            _dataContext.SaveChanges();
            return entity;
        }

        public IList<CommentReply> GetAllReplies()
        {
            return _dbSet.Where(x => !x.IsDeleted).AsNoTracking().ToList();
        }

        public IList<CommentReply> GetApprovedReplies(int commentId)
        {
            return _dbSet.Where(x => !x.IsDeleted && x.CommentId == commentId && x.IsApproved).OrderByDescending(x => x.DateCreated).ToList();
        }

        public CommentReply GetCommentReply(int commentReplyId)
        {
            return _dbSet.Where(x => x.Id == commentReplyId).AsNoTracking().ToList().First();
        }

        public CommentReply Update(CommentReply entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
            _dataContext.SaveChanges();
            return entity;
        }
    }
}
