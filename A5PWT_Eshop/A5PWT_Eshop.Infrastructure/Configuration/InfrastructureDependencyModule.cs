﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Infrastructure.Configuration
{
    public class InfrastructureDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(ThisAssembly).AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
