﻿using A5PWT_Eshop.Infrastructure.Data;
using A5PWT_Eshop.Infrastructure.Identity.Roles;
using A5PWT_Eshop.Infrastructure.Identity.Users;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace A5PWT_Eshop.Infrastructure.Configuration
{
    public class InfrastructureServiceConfiguration
    {
        public static void Load(IServiceCollection services, IHostingEnvironment environment)
        {
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlServer($"Server=databaze.fai.utb.cz;Database=A17133_A5PWT;User ID=A17133;Password=A17133A17133;persist security info = True;" +
                $"multipleactiveresultsets=True");
            });
            services.AddIdentity<User, Role>().AddEntityFrameworkStores<DataContext>().AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;

                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                options.LoginPath = "/Admin/Security/Login";
                options.LogoutPath = "/Admin/Security/Logout";
                options.SlidingExpiration = true;
            });
        }
    }
}
