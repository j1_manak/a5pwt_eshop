﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace A5PWT_Eshop.Infrastructure.Migrations
{
    public partial class uprava_comments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Author",
                table: "CommentReplies",
                newName: "AuthorName");

            migrationBuilder.AddColumn<string>(
                name: "AuthorName",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ParentCommentId",
                table: "CommentReplies",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuthorName",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "ParentCommentId",
                table: "CommentReplies");

            migrationBuilder.RenameColumn(
                name: "AuthorName",
                table: "CommentReplies",
                newName: "Author");
        }
    }
}
