﻿using A5PWT_Eshop.Infrastructure.Configuration;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace A5PWT_Eshop.Application.Configuration
{
    public class ServiceConfiguration
    {
        public static void Load(IServiceCollection services, IHostingEnvironment environment)
        {
            services.AddAutoMapper(Assembly.GetAssembly(typeof(ServiceConfiguration)));
            services.AddSingleton(environment);
            InfrastructureServiceConfiguration.Load(services, environment);
        }
    }
}
