﻿using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Products;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Configuration.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMaps();
        }
        private void CreateMaps()
        {
            CreateMap<Product, ProductViewModel>().ReverseMap();
        }
    }
}
