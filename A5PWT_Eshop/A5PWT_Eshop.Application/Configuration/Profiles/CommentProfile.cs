﻿using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Comments;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Configuration.Profiles
{
    public class CommentProfile : Profile
    {
        public CommentProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<Comment, CommentViewModel>()
                .ForMember(dest => dest.CreatorId, opt => opt.MapFrom(x => x.ProductId))
                .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(x => x.AuthorName))
                .ForMember(dest => dest.CreationTime, opt => opt.MapFrom(x => x.DateCreated))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(x => x.Text))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(x => x.Title))
                .ForMember(dest => dest.IsApproved, opt => opt.MapFrom(x => x.IsApproved))
                .ReverseMap();
        }
    }
}
