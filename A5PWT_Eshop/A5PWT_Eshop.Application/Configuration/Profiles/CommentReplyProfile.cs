﻿using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Comments;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Configuration.Profiles
{
    class CommentReplyProfile : Profile
    {
        public CommentReplyProfile()
        {
            GenerateMaps();
        }

        private void GenerateMaps()
        {
            CreateMap<CommentReply, CommentReplyViewModel>()
                .ForMember(dest => dest.Author, opt => opt.MapFrom(x => x.AuthorName))
                .ForMember(dest => dest.CreationTime, opt => opt.MapFrom(x => x.DateCreated))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(x => x.Text))
                .ForMember(dest => dest.IsApproved, opt => opt.MapFrom(x => x.IsApproved))
                .ForMember(dest => dest.ParentCommentId, opt => opt.MapFrom(x => x.CommentId))
                .ReverseMap();
        }
    }
}
