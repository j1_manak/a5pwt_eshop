﻿using A5PWT_Eshop.Application.ViewModels.Carts;
using A5PWT_Eshop.Domain.Entities.Carts;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Configuration.Profiles
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<CartItem, CartItemViewModel>()
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Product.ImageUrl))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.Name))
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.Product.Id))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Product.Price));
        }
    }
}
