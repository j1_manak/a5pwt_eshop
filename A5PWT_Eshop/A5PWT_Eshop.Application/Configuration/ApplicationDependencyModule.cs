﻿using A5PWT_Eshop.Domain.Configuration;
using A5PWT_Eshop.Infrastructure.Configuration;
using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Configuration
{
    public class ApplicationDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(ThisAssembly).AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterModule<DomainDependencyModule>();
            builder.RegisterModule<InfrastructureDependencyModule>();
        }
    }
}
