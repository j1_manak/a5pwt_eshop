﻿
using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Mappers.Comments
{
    public interface ICommentMapper
    {
        CommentViewModel GetViewModelFromEntity(Comment entity);
        Comment GetEntityFromViewModel(CommentViewModel vm);
        IList<CommentViewModel> GetViewModelsFromEntities(IList<Comment> entities);
        IList<Comment> GetEntitiesFromViewModels(IList<CommentViewModel> vms);
    }
}
