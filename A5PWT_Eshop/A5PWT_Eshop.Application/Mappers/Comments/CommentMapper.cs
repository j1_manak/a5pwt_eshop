﻿using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Comments;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Mappers.Comments
{
    public class CommentMapper : ICommentMapper
    {
        private readonly IMapper _mapper;

        public CommentMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IList<Comment> GetEntitiesFromViewModels(IList<CommentViewModel> vms)
        {
            return _mapper.Map<IList<Comment>>(vms);
        }

        public Comment GetEntityFromViewModel(CommentViewModel vm)
        {
            return _mapper.Map<Comment>(vm);
        }

        public CommentViewModel GetViewModelFromEntity(Comment entity)
        {
            return _mapper.Map<CommentViewModel>(entity);
        }

        public IList<CommentViewModel> GetViewModelsFromEntities(IList<Comment> entities)
        {
            return _mapper.Map<IList<CommentViewModel>>(entities);
        }
    }
}
