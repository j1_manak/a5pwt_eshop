﻿using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Comments;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Mappers.Comments
{
    public class CommentReplyMapper : ICommentReplyMapper
    {
        private readonly IMapper _mapper;

        public CommentReplyMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IList<CommentReply> GetEntitiesFromViewModels(IList<CommentReplyViewModel> vms)
        {
            return _mapper.Map<IList<CommentReply>>(vms);
        }

        public CommentReply GetEntityFromViewModel(CommentReplyViewModel vm)
        {
            return _mapper.Map<CommentReply>(vm);
        }

        public CommentReplyViewModel GetViewModelFromEntity(CommentReply entity)
        {
            return _mapper.Map<CommentReplyViewModel>(entity);
        }

        public IList<CommentReplyViewModel> GetViewModelsFromEntities(IList<CommentReply> entities)
        {
            return _mapper.Map<IList<CommentReplyViewModel>>(entities);
        }
    }
}
