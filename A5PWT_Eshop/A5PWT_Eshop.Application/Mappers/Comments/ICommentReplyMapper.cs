﻿using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Mappers.Comments
{
    public interface ICommentReplyMapper
    {
        CommentReplyViewModel GetViewModelFromEntity(CommentReply entity);
        CommentReply GetEntityFromViewModel(CommentReplyViewModel vm);
        IList<CommentReplyViewModel> GetViewModelsFromEntities(IList<CommentReply> entities);
        IList<CommentReply> GetEntitiesFromViewModels(IList<CommentReplyViewModel> vms);
    }
}
