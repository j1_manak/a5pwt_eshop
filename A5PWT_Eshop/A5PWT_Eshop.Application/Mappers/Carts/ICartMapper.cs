﻿using A5PWT_Eshop.Application.ViewModels.Carts;
using A5PWT_Eshop.Domain.Entities.Carts;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Mappers.Carts
{
    public interface ICartMapper
    {
        IList<CartItemViewModel> GetViewModelsFromEntities(IList<CartItem> entities);
    }
}
