﻿using System;
using System.Collections.Generic;
using System.Text;
using A5PWT_Eshop.Application.ViewModels.Carts;
using A5PWT_Eshop.Domain.Entities.Carts;
using AutoMapper;

namespace A5PWT_Eshop.Application.Mappers.Carts
{
    public class CartMapper : ICartMapper
    {
        private readonly IMapper _mapper;

        public CartMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IList<CartItemViewModel> GetViewModelsFromEntities(IList<CartItem> entities)
        {
            return _mapper.Map<IList<CartItemViewModel>>(entities);
        }
    }
}
