﻿using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.Mappers.Products
{
    public interface IProductMapper
    {
        ProductViewModel GetViewModelFromEntitiy(Product entity);
        Product GetEntityFromViewModel(ProductViewModel productViewModel);
        IList<ProductViewModel> GetViewModelsFromEntities(IList<Product> entities);
    }
}
