﻿using System;
using System.Collections.Generic;
using System.Text;
using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Products;
using AutoMapper;

namespace A5PWT_Eshop.Application.Mappers.Products
{
    public class ProductMapper : IProductMapper
    {
        private readonly IMapper _mapper;

        public ProductMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Product GetEntityFromViewModel(ProductViewModel productViewModel)
        {
            return _mapper.Map<Product>(productViewModel);
        }

        public ProductViewModel GetViewModelFromEntitiy(Product entity)
        {
            return _mapper.Map<ProductViewModel>(entity);
        }

        public IList<ProductViewModel> GetViewModelsFromEntities(IList<Product> entities)
        {
            return _mapper.Map<IList<ProductViewModel>>(entities);
        }
    }
}
