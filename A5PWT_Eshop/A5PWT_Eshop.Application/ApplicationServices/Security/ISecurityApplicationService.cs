﻿using A5PWT_Eshop.Application.ViewModels.Security;
using A5PWT_Eshop.Infrastructure.Identity.Users;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace A5PWT_Eshop.Application.ApplicationServices.Security
{
    public interface ISecurityApplicationService
    {
        Task RegisterAndLogin(LoginViewModel viewModel);
        Task Logout();
        Task<bool> Login(LoginViewModel viewModel);
        Task<User> GetCurrentUser(ClaimsPrincipal principal);
        Task<IList<string>> GetUserRoles(User user);
        Task<User> GetUserByEmail(string email);
    }
}
