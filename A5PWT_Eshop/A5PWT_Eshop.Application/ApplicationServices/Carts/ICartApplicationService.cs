﻿using A5PWT_Eshop.Application.ViewModels.Carts;
using A5PWT_Eshop.Domain.Entities.Carts;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ApplicationServices.Carts
{
    public interface ICartApplicationService
    {
        IndexViewModel GetIndexViewModel(string userTrackingCode);
        CartItem AddToCart(int productId, int amount, string userTrackingCode);
        void RemoveFromCart(int productId, string userTrackingCode);
        IList<CartItem> GetCartItems(string userTrackingCode);
    }
}
