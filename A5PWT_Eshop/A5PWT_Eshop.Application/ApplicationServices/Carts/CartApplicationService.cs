﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using A5PWT_Eshop.Application.Mappers.Carts;
using A5PWT_Eshop.Application.ViewModels.Carts;
using A5PWT_Eshop.Domain.Entities.Carts;
using A5PWT_Eshop.Domain.Services.Carts;

namespace A5PWT_Eshop.Application.ApplicationServices.Carts
{
    public class CartApplicationService : ICartApplicationService
    {
        private readonly ICartService _cartService;
        private readonly ICartMapper _cartMapper;

        public CartApplicationService(ICartService cartService, ICartMapper cartMapper)
        {
            _cartService = cartService;
            _cartMapper = cartMapper;
        }

        public CartItem AddToCart(int productId, int amount, string userTrackingCode)
        {
            return _cartService.AddToCart(productId, amount, userTrackingCode);
        }

        public IList<CartItem> GetCartItems(string userTrackingCode)
        {
            return _cartService.GetCartItems(userTrackingCode);
        }

        public IndexViewModel GetIndexViewModel(string userTrackingCode)
        {
            var items = _cartMapper.GetViewModelsFromEntities(_cartService.GetCartItems(userTrackingCode));
            var vm = new IndexViewModel();
            vm.CartsItem = items;
            vm.TotalPrice = items.Sum(x => x.Price * x.Amount);
            return vm;
        }

        public void RemoveFromCart(int productId, string userTrackingCode)
        {
            _cartService.RemoveFromCart(productId, userTrackingCode);
        }
    }
}
