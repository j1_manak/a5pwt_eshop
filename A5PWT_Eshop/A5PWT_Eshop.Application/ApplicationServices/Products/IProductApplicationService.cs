﻿using A5PWT_Eshop.Application.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ApplicationServices.Products
{
    public interface IProductApplicationService
    {
        IndexViewModel GetIndexViewModel();
        ProductViewModel GetProductViewModel(int id);
        ProductViewModel Insert(ProductViewModel model);
        ProductViewModel Edit(ProductViewModel model);
        ProductViewModel Delete(ProductViewModel model);
    }
}
