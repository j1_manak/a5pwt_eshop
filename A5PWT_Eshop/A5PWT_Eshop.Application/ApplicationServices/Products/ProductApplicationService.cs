﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using A5PWT_Eshop.Application.Mappers.Products;
using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Entities.Products;
using A5PWT_Eshop.Domain.Services.Files;
using A5PWT_Eshop.Domain.Services.Products;
using AutoMapper;

namespace A5PWT_Eshop.Application.ApplicationServices.Products
{
    public class ProductApplicationService : IProductApplicationService
    {
        private readonly IProductMapper _productMapper;
        private readonly IProductService _productService;
        private readonly IFileHandler _fileHandler;

        public ProductApplicationService(IProductMapper productMapper, IProductService productService, IFileHandler fileHandler)
        {
            _productMapper = productMapper;
            _productService = productService;
            _fileHandler = fileHandler;
        }

        public ProductViewModel Delete(ProductViewModel model)
        {
            var entity = _productMapper.GetEntityFromViewModel(model);
            entity = _productService.Delete(entity);
            return _productMapper.GetViewModelFromEntitiy(entity);
        }

        public ProductViewModel Edit(ProductViewModel model)
        {
            var entity = _productMapper.GetEntityFromViewModel(model);
            if (model.Image != null)
            {
                entity.ImageUrl = _fileHandler.SaveImage(model.Image);
            }
            entity = _productService.Update(entity);
            return _productMapper.GetViewModelFromEntitiy(entity);
        }

        public IndexViewModel GetIndexViewModel()
        {
            return new IndexViewModel
            {
                Products = _productMapper.GetViewModelsFromEntities(_productService.GetAll()).ToList()
            };
        }

        public ProductViewModel GetProductViewModel(int id)
        {
            Product entity = _productService.Get(x => x.Id == id);
            return _productMapper.GetViewModelFromEntitiy(entity);
        }

        public ProductViewModel Insert(ProductViewModel model)
        {
            var entity = _productMapper.GetEntityFromViewModel(model);
            if(model.Image != null)
            {
                entity.ImageUrl = _fileHandler.SaveImage(model.Image);
            }
            return _productMapper.GetViewModelFromEntitiy(_productService.Insert(entity));
        }
    }
}
