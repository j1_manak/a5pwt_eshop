﻿using A5PWT_Eshop.Application.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ApplicationServices.Orders
{
    public interface IOrderApplicationService
    {
        void CreateOrder(int userId, string userTrackingCode);
        IndexViewModel GetIndexViewModel(int userID);
    }
}
