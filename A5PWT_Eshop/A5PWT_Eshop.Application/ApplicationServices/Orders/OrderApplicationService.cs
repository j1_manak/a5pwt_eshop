﻿using A5PWT_Eshop.Application.ViewModels.Orders;
using A5PWT_Eshop.Domain.Services.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A5PWT_Eshop.Application.ApplicationServices.Orders
{
    class OrderApplicationService : IOrderApplicationService
    {
        private readonly IOrderService _orderService;

        public OrderApplicationService(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public void CreateOrder(int userId, string userTrackingCode)
        {
            _orderService.CreateOrder(userId, userTrackingCode);
        }

        public IndexViewModel GetIndexViewModel(int userID)
        {
            var orders = _orderService.GetOrders(userID);
            var ordersViewModels = new List<OrderViewModel>();
            foreach (var order in orders)
            {
                ordersViewModels.Add(new OrderViewModel
                {
                    DateCreated = order.DateCreated,
                    ItemCount = order.OrderItems.Count,
                    Total = order.OrderItems.Sum(x => x.Price * x.Amount)
                });
            }

            return new IndexViewModel { Orders = ordersViewModels };
        }
    }
}
