﻿using A5PWT_Eshop.Application.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ApplicationServices.Comments
{
    public interface ICommentApplicationService
    {
        CommentViewModel AddComment(CommentViewModel model);
        CommentViewModel EditComment(CommentViewModel model);
        CommentViewModel DeleteComment(CommentViewModel model);
        IList<CommentViewModel> GetApprovedComments(int productId);
        IList<CommentViewModel> GetAllComments();
        CommentViewModel GetComment(int commentId);
    }
}
