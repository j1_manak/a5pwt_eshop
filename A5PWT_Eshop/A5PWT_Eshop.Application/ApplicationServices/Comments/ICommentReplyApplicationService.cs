﻿using A5PWT_Eshop.Application.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ApplicationServices.Comments
{
    public interface ICommentReplyApplicationService
    {
        CommentReplyViewModel AddCommentReply(CommentReplyViewModel model);
        CommentReplyViewModel DeleteCommentReply(CommentReplyViewModel model);
        CommentReplyViewModel EditCommentReply(CommentReplyViewModel model);
        IList<CommentReplyViewModel> GetApprovedCommentReplies(int commentId);
        IList<CommentReplyViewModel> GetAllReplies();
        CommentReplyViewModel GetCommentReply(int commentReplyId);
    }
}
