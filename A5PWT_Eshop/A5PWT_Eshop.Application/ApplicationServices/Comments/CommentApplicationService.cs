﻿using A5PWT_Eshop.Application.Mappers.Comments;
using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Services.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ApplicationServices.Comments
{
    public class CommentApplicationService : ICommentApplicationService
    {
        private readonly ICommentMapper _commentMapper;
        private readonly ICommentService _commentService;

        public CommentApplicationService(ICommentMapper commentMapper, ICommentService commentService)
        {
            _commentMapper = commentMapper;
            _commentService = commentService;
        }

        public CommentViewModel AddComment(CommentViewModel model)
        {
            var entity = _commentMapper.GetEntityFromViewModel(model);
            entity = _commentService.Add(entity);
            return _commentMapper.GetViewModelFromEntity(entity);
        }

        public CommentViewModel DeleteComment(CommentViewModel model)
        {
            var entity = _commentMapper.GetEntityFromViewModel(model);
            entity = _commentService.Delete(entity);
            return _commentMapper.GetViewModelFromEntity(entity);
        }

        public CommentViewModel EditComment(CommentViewModel model)
        {
            var entity = _commentMapper.GetEntityFromViewModel(model);
            entity = _commentService.Update(entity);
            return _commentMapper.GetViewModelFromEntity(entity);
        }

        public IList<CommentViewModel> GetAllComments()
        {
            var entities = _commentService.GetAllComments();
            return _commentMapper.GetViewModelsFromEntities(entities);
        }

        public IList<CommentViewModel> GetApprovedComments(int productId)
        {
            var entities = _commentService.GetApprovedComments(productId);
            return _commentMapper.GetViewModelsFromEntities(entities);
        }

        public CommentViewModel GetComment(int commentId)
        {
            var entity = _commentService.GetComment(commentId);
            return _commentMapper.GetViewModelFromEntity(entity);
        }
    }
}
