﻿using A5PWT_Eshop.Application.Mappers.Comments;
using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Domain.Services.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ApplicationServices.Comments
{
    public class CommentReplyApplicationService : ICommentReplyApplicationService
    {
        private readonly ICommentReplyMapper _commentReplyMapper;
        private readonly ICommentReplyService _commentReplyService;

        public CommentReplyApplicationService(ICommentReplyMapper commentReplyMapper, ICommentReplyService commentReplyService)
        {
            _commentReplyMapper = commentReplyMapper;
            _commentReplyService = commentReplyService;
        }

        public CommentReplyViewModel AddCommentReply(CommentReplyViewModel model)
        {
            var entity = _commentReplyMapper.GetEntityFromViewModel(model);
            entity = _commentReplyService.Add(entity);
            return _commentReplyMapper.GetViewModelFromEntity(entity);
        }

        public CommentReplyViewModel DeleteCommentReply(CommentReplyViewModel model)
        {
            var entity = _commentReplyMapper.GetEntityFromViewModel(model);
            entity = _commentReplyService.Delete(entity);
            return _commentReplyMapper.GetViewModelFromEntity(entity);
        }

        public CommentReplyViewModel EditCommentReply(CommentReplyViewModel model)
        {
            var entity = _commentReplyMapper.GetEntityFromViewModel(model);
            entity = _commentReplyService.Update(entity);
            return _commentReplyMapper.GetViewModelFromEntity(entity);
        }

        public IList<CommentReplyViewModel> GetAllReplies()
        {
            var entities = _commentReplyService.GetAllReplies();
            return _commentReplyMapper.GetViewModelsFromEntities(entities);
        }

        public IList<CommentReplyViewModel> GetApprovedCommentReplies(int commentId)
        {
            var entities = _commentReplyService.GetApprovedReplies(commentId);
            return _commentReplyMapper.GetViewModelsFromEntities(entities);
        }

        public CommentReplyViewModel GetCommentReply(int commentReplyId)
        {
            var entity = _commentReplyService.GetCommentReply(commentReplyId);
            return _commentReplyMapper.GetViewModelFromEntity(entity);
        }
    }
}
