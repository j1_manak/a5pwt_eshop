﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.Products
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public int CreatorId { get; set; }
        public string AuthorName { get; set; }
        public DateTime CreationTime { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public bool IsApproved { get; set; }
        public IList<CommentReplyViewModel> CommentReplies { get; set; }
    }
}
