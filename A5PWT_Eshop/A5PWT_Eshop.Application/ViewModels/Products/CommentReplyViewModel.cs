﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.Products
{
    public class CommentReplyViewModel
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public DateTime CreationTime { get; set; }
        public string Text { get; set; }
        public bool IsApproved { get; set; }
        public int ParentCommentId { get; set; }
    }
}
