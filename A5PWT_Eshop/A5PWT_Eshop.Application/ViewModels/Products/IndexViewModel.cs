﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.Products
{
    public class IndexViewModel
    {
        public List<ProductViewModel> Products { get; set; }
    }
}
