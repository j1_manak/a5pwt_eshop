﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.Products
{
    public class ProductViewModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public IFormFile Image { get; set; }
        public IList<CommentViewModel> Comments { get; set; }
        public int? UserId { get; set; }
        public string UserEmail { get; set; }

    }
}
