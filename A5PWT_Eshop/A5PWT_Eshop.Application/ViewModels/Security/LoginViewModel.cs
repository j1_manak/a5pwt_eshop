﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.Security
{
    public class LoginViewModel
    {
        public string  Login { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
