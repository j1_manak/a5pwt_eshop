﻿using A5PWT_Eshop.Application.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.CommentReplies
{
    public class IndexViewModel
    {
        public IList<CommentReplyViewModel> Replies { get; set; }
    }
}
