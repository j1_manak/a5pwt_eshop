﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.Orders
{
    public class OrderViewModel
    {
        public DateTime DateCreated { get; set; }
        public decimal Total { get; set; }
        public int ItemCount { get; set; }
    }
}
