﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.Orders
{
    public class IndexViewModel
    {
        public IList<OrderViewModel> Orders { get; set; }
    }
}
