﻿using A5PWT_Eshop.Domain.Entities.Carts;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Application.ViewModels.Carts
{
    public class IndexViewModel
    {
        public IList<CartItemViewModel> CartsItem { get; set; }
        public decimal TotalPrice { get; set; }
        public int? UserID { get; set; }
        public string  UserEmail { get; set; }
    }
}
