﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace A5PWT_Eshop.Domain.Services.Files
{
    public class FileHandler : IFileHandler
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public FileHandler(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public string SaveImage(IFormFile file)
        {
            var relativePath = $"images/products/{Guid.NewGuid()}.{file.ContentType.Replace("image/", string.Empty)}";
            var absolutePath = Path.Combine(_hostingEnvironment.WebRootPath, relativePath);
            using(FileStream stream = new FileStream(absolutePath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            return $"/{relativePath}";
        }
    }
}
