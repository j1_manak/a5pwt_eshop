﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Services.Files
{
    public interface IFileHandler
    {
        string SaveImage(IFormFile file);
        
    }
}
