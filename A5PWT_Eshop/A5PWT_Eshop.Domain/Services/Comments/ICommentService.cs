﻿using A5PWT_Eshop.Domain.Entities.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Services.Comments
{
    public interface ICommentService
    {
        IList<Comment> GetApprovedComments(int productId);
        IList<Comment> GetAllComments();
        Comment GetComment(int commentId);
        Comment Add(Comment comment);
        Comment Update(Comment comment);
        Comment Delete(Comment comment);
    }
}
