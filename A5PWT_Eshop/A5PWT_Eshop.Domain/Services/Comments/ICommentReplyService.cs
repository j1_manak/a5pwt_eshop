﻿using A5PWT_Eshop.Domain.Entities.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Services.Comments
{
    public interface ICommentReplyService
    {
        IList<CommentReply> GetApprovedReplies(int commentId);
        IList<CommentReply> GetAllReplies();
        CommentReply GetCommentReply(int commentReplyId);
        CommentReply Add(CommentReply commentReply);
        CommentReply Delete(CommentReply commentReply);
        CommentReply Update(CommentReply commentReply);
    }
}
