﻿using A5PWT_Eshop.Domain.Entities.Comments;
using A5PWT_Eshop.Domain.Repositories.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Services.Comments
{
    class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public Comment Add(Comment comment)
        {
            return _commentRepository.Add(comment);
        }

        public Comment Delete(Comment comment)
        {
            return _commentRepository.Delete(comment);
        }

        public IList<Comment> GetAllComments()
        {
            return _commentRepository.GetAllComments();
        }

        public IList<Comment> GetApprovedComments(int productId)
        {
            return _commentRepository.GetApprovedComments(productId);
        }

        public Comment GetComment(int commentId)
        {
            return _commentRepository.GetComment(commentId);
        }

        public Comment Update(Comment comment)
        {
            return _commentRepository.Update(comment);
        }
    }
}
