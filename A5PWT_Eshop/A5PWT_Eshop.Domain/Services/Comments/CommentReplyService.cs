﻿using A5PWT_Eshop.Domain.Entities.Comments;
using A5PWT_Eshop.Domain.Repositories.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Services.Comments
{
    public class CommentReplyService : ICommentReplyService
    {
        private readonly ICommentReplyRepository _commentReplyRepository;

        public CommentReplyService(ICommentReplyRepository commentReplyRepository)
        {
            _commentReplyRepository = commentReplyRepository;
        }

        public CommentReply Add(CommentReply commentReply)
        {
            return _commentReplyRepository.Add(commentReply);
        }

        public CommentReply Delete(CommentReply commentReply)
        {
            return _commentReplyRepository.Delete(commentReply);
        }

        public IList<CommentReply> GetAllReplies()
        {
            return _commentReplyRepository.GetAllReplies();
        }

        public IList<CommentReply> GetApprovedReplies(int commentId)
        {
            return _commentReplyRepository.GetApprovedReplies(commentId);
        }

        public CommentReply GetCommentReply(int commentReplyId)
        {
            return _commentReplyRepository.GetCommentReply(commentReplyId);
        }

        public CommentReply Update(CommentReply commentReply)
        {
            return _commentReplyRepository.Update(commentReply);
        }
    }
}
