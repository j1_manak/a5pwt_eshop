﻿using A5PWT_Eshop.Domain.Entities.Orders;
using A5PWT_Eshop.Domain.Repositories.Orders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Services.Orders
{
    class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void CreateOrder(int userId, string userTrackingCode)
        {
            _orderRepository.CreateOrder(userId, userTrackingCode);
        }

        public IList<Order> GetOrders(int userID)
        {
            return _orderRepository.GetOrders(userID);
        }
    }
}
