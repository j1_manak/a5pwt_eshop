﻿using A5PWT_Eshop.Domain.Entities.Orders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Services.Orders
{
    public interface IOrderService
    {
        void CreateOrder(int userId, string userTrackingCode);
        IList<Order> GetOrders(int userID);
    }
}
