﻿using A5PWT_Eshop.Domain.Entities.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Repositories.Products
{
    public interface IProductRepository : IRepository<Product>
    {

    }
}
