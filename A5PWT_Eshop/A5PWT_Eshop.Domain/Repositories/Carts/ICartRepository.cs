﻿using A5PWT_Eshop.Domain.Entities.Carts;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Repositories.Carts
{
    public interface ICartRepository
    {
        CartItem AddToCart(int productID, int amount, string userTrackingCode);
        void RemoveFromCart(int productID, string userTrackingCode);
        IList<CartItem> GetCartItems(string userTrackingCode);
        Cart GetCurrentCart(string userTrackingCode);
    }
}
