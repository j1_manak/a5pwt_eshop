﻿using A5PWT_Eshop.Domain.Entities.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Repositories.Comments
{
    public interface ICommentRepository
    {
        IList<Comment> GetApprovedComments(int productId);
        IList<Comment> GetAllComments();
        Comment GetComment(int commentId);
        Comment Add(Comment entity);
        Comment Update(Comment entity);
        Comment Delete(Comment entity);
    }
}
