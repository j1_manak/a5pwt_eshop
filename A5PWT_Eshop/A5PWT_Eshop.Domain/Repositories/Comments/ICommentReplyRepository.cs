﻿using A5PWT_Eshop.Domain.Entities.Comments;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Repositories.Comments
{
    public interface ICommentReplyRepository
    {
        IList<CommentReply> GetApprovedReplies(int commentId);
        IList<CommentReply> GetAllReplies();
        CommentReply GetCommentReply(int commentReplyId);
        CommentReply Add(CommentReply entity);
        CommentReply Delete(CommentReply entity);
        CommentReply Update(CommentReply entity);
    }
}
