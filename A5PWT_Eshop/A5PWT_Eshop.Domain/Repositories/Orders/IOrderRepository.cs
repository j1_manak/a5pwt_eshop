﻿using A5PWT_Eshop.Domain.Entities.Orders;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Repositories.Orders
{
    public interface IOrderRepository
    {
        void CreateOrder(int userId, string userTrackingCode);
        IList<Order> GetOrders(int userID);
    }
}
