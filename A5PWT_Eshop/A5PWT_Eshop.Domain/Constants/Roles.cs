﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Constants
{
    public class Roles
    {
        public static readonly string User = "User";
        public static readonly string Admin = "Admin";
        public static readonly string Manager = "Manager";
    }
}
