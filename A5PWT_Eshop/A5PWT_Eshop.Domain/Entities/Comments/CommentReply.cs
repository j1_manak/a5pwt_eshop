﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Entities.Comments
{
    public class CommentReply : Entity
    {
        public int CommentId { get; set; }
        public string AuthorName { get; set; }
        public string Text { get; set; }
        public int ParentCommentId { get; set; }
        public bool IsApproved { get; set; }
    }
}
