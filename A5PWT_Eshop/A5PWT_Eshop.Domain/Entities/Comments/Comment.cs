﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Entities.Comments
{
    public class Comment : Entity
    {
        public int ProductId { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public IList<CommentReply> Replies { get; set; }
        public bool IsApproved { get; set; }
    }
}
