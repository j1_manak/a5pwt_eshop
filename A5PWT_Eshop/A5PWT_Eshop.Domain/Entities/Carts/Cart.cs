﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Entities.Carts
{
    public class Cart : Entity
    {
        public string UserTrackingCode { get; set; }
        public IList<CartItem> CartItems { get; set; }
    }
}
