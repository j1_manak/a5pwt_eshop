﻿using A5PWT_Eshop.Domain.Entities.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Entities.Carts
{
    public class CartItem : Entity
    {
        public int CartId { get; set; }
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public Cart Cart { get; set; }
        public Product Product { get; set; }
    }
}
