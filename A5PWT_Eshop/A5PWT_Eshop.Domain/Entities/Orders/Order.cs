﻿using A5PWT_Eshop.Domain.Entities.Addresses;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Entities.Orders
{
    public class Order : Entity
    {
        public int UserId { get; set; }
        public string OrderNumber { get; set; }
        public int? ShippingAddressId { get; set; }
        public int? BillingAddressId { get; set; }

        public IList<OrderItem> OrderItems { get; set; }
        public Address BillingAddress { get; set; }
        public Address ShippingAddress { get; set; }
    }
}
