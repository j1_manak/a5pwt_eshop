﻿using A5PWT_Eshop.Domain.Entities.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace A5PWT_Eshop.Domain.Entities.Orders
{
    public class OrderItem :Entity
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public Order Order { get; set; }
        public Product Product { get; set; }
    }
}
