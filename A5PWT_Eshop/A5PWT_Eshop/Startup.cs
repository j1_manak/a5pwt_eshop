﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using A5PWT_Eshop.Application.Configuration;
using A5PWT_Eshop.Domain.Constants;
using A5PWT_Eshop.Infrastructure.Identity.Roles;
using A5PWT_Eshop.Infrastructure.Identity.Users;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace A5PWT_Eshop
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IHostingEnvironment Environment { get; set; }
        public IContainer Container { get; private set; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            /*services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });*/

            services.AddMvc().AddControllersAsServices();
            ServiceConfiguration.Load(services, Environment);
            return ConfigureAutofacContainer(services);
        }

        private IServiceProvider ConfigureAutofacContainer(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<ApplicationDependencyModule>();
            builder.Populate(services);

            Container = builder.Build();
            return new AutofacServiceProvider(Container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                   name: "areas",
                   template: "{area:exists}/{controller=Products}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Products}/{action=Index}/{id?}");
                
            });

            EnsureRolesCreated(app).ConfigureAwait(false);
        }

        private async Task EnsureRolesCreated(IApplicationBuilder app)
        {
            using(var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<Role>>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<User>>();
                string[] roles = { Roles.User, Roles.Admin, Roles.Manager };
                foreach (var role in roles)
                {
                    var roleExists = await roleManager.RoleExistsAsync(role);
                    if (!roleExists)
                        await roleManager.CreateAsync(new Role(role));
                }

                var admin = new User
                {
                    UserName = "admin@utb.cz",
                    Email = "admin@utb.cz",
                    EmailConfirmed = true
                };
                var passwd = "Heslo_123";
                var user = await userManager.FindByEmailAsync(admin.Email);
                if(user == null)
                {
                    var createdUser = await userManager.CreateAsync(admin, passwd);
                    if(createdUser.Succeeded)
                    {
                        foreach (var role in roles)
                        {
                            await userManager.AddToRoleAsync(admin, role);
                        }
                    }
                }
            }
        }
    }
}
