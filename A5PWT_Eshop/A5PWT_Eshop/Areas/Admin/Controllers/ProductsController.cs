﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using A5PWT_Eshop.Application.ApplicationServices.Products;
using A5PWT_Eshop.Application.ViewModels.Products;
using A5PWT_Eshop.Areas.Admin.Controllers.Common;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace A5PWT_Eshop.Areas.Admin.Controllers
{
    public class ProductsController : AdminController
    {
        private readonly IProductApplicationService _productApplicationService;
        public ProductsController(IProductApplicationService productApplicationService)
        {
            _productApplicationService = productApplicationService;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            
            var vm = _productApplicationService.GetIndexViewModel();
            return View(vm);
        }

        public IActionResult Edit(int id)
        {
            var vm = _productApplicationService.GetProductViewModel(id);
            return View(vm);
        }

        public IActionResult Delete(ProductViewModel model)
        {
            _productApplicationService.Delete(model);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult Edit(ProductViewModel model)
        {
            var product = _productApplicationService.Edit(model);
            return Json(product);
        }

        public IActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Insert(ProductViewModel model)
        {
            var product = _productApplicationService.Insert(model);
            return Json(product);
        }

    }
}
