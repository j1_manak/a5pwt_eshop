﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using A5PWT_Eshop.Application.ApplicationServices.Comments;
using A5PWT_Eshop.Application.ViewModels.CommentReplies;
using A5PWT_Eshop.Areas.Admin.Controllers.Common;
using Microsoft.AspNetCore.Mvc;

namespace A5PWT_Eshop.Areas.Admin.Controllers
{
    public class CommentsRepliesController : AdminController
    {
        private readonly ICommentReplyApplicationService _commentReplyApplicationService;

        public CommentsRepliesController(ICommentReplyApplicationService commentReplyApplicationService)
        {
            _commentReplyApplicationService = commentReplyApplicationService;
        }

        public IActionResult Index()
        {
            IndexViewModel indexViewModel = new IndexViewModel();
            indexViewModel.Replies = _commentReplyApplicationService.GetAllReplies();
            return View(indexViewModel);
        }

        [HttpPost]
        public IActionResult Approve(int commentReplyId)
        {
            var reply = _commentReplyApplicationService.GetCommentReply(commentReplyId);
            reply.IsApproved = true;
            reply = _commentReplyApplicationService.EditCommentReply(reply);

            return RedirectToAction("Index", "CommentsReplies", new { Area = "Admin" });
        }

        [HttpPost]
        public IActionResult Delete(int commentReplyId)
        {
            var reply = _commentReplyApplicationService.GetCommentReply(commentReplyId);
            reply = _commentReplyApplicationService.DeleteCommentReply(reply);
            return RedirectToAction("Index", "CommentsReplies", new { Area = "Admin" });
        }
    }
}