﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace A5PWT_Eshop.Areas.Admin.Controllers.Common
{
    [Authorize(Roles = "Manager, Admin"), Area("Admin")]
    public class AdminController : Controller
    {
        
    }
}