﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using A5PWT_Eshop.Application.ApplicationServices.Comments;
using A5PWT_Eshop.Application.ApplicationServices.Security;
using A5PWT_Eshop.Application.ViewModels.Comments;
using A5PWT_Eshop.Areas.Admin.Controllers.Common;
using Microsoft.AspNetCore.Mvc;

namespace A5PWT_Eshop.Areas.Admin.Controllers
{
    public class CommentsController : AdminController
    {
        private readonly ICommentApplicationService _commentApplicationService;

        public CommentsController(ICommentApplicationService commentApplicationService)
        {
            _commentApplicationService = commentApplicationService;
        }

        public IActionResult Index()
        {
            IndexViewModel vm = new IndexViewModel();
            vm.Comments = _commentApplicationService.GetAllComments();
            return View(vm);
        }

        [HttpPost]
        public IActionResult Approve(int commentID)
        {
            //var comment = _commentApplicationService.GetComment(commentID);
            var comments = _commentApplicationService.GetAllComments();
            var comment = comments.FirstOrDefault(x => x.Id == commentID);
            comment.IsApproved = true;
            comment =  _commentApplicationService.EditComment(comment);

            return RedirectToAction("Index", "Comments", new { Area = "Admin"});
        }

        [HttpPost]
        public IActionResult Delete(int commentID)
        {
            var comment = _commentApplicationService.GetComment(commentID);
            comment = _commentApplicationService.DeleteComment(comment);
            return RedirectToAction("Index", "Comments", new { Area = "Admin" });
        }
    }
}