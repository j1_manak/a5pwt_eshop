﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using A5PWT_Eshop.Application.ApplicationServices.Comments;
using A5PWT_Eshop.Application.ApplicationServices.Products;
using A5PWT_Eshop.Application.ApplicationServices.Security;
using A5PWT_Eshop.Application.ViewModels.Products;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace A5PWT_Eshop.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductApplicationService _productApplicationService;
        private readonly ICommentApplicationService _commentApplicationService;
        private readonly ICommentReplyApplicationService _commentReplyApplicationService;
        private readonly ISecurityApplicationService _securityApplicationService;
        public ProductsController(IProductApplicationService productApplicationService, ICommentApplicationService commentApplicationService,
            ICommentReplyApplicationService commentReplyApplicationService, ISecurityApplicationService securityApplicationService)
        {
            _productApplicationService = productApplicationService;
            _commentApplicationService = commentApplicationService;
            _commentReplyApplicationService = commentReplyApplicationService;
            _securityApplicationService = securityApplicationService;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var vm = _productApplicationService.GetIndexViewModel();
            return View(vm);
        }

        public async Task<IActionResult> Detail(int id)
        {
            var vm = _productApplicationService.GetProductViewModel(id);
            vm.Comments = _commentApplicationService.GetApprovedComments(vm.Id);
            for(int i = 0; i < vm.Comments.Count; i++)
            {
                vm.Comments[i].CommentReplies = _commentReplyApplicationService.GetApprovedCommentReplies(vm.Comments[i].Id);
            }
            var user = await _securityApplicationService.GetCurrentUser(User);
            if (user != null)
            {
                vm.UserEmail = user.Email;
                vm.UserId = user.Id;
            }
            return View(vm);
        }

        [HttpPost]
        public IActionResult AddComment(int productId, string title, string text, string authorName)
        {
            CommentViewModel comment = new CommentViewModel
            {
                CreationTime = DateTime.Now,
                CreatorId = productId,
                AuthorName = authorName,
                Text = text,
                Title = title,
                IsApproved = false,
                CommentReplies = new List<CommentReplyViewModel>()
            };
            _commentApplicationService.AddComment(comment);
            var vm = _productApplicationService.GetProductViewModel(productId);
            return RedirectToAction("Detail", "Products", new { ProductId = productId});
        }

        [HttpPost]
        public IActionResult AddReply(int commentId, string text, int productId, string authorName)
        {
            CommentReplyViewModel commentReply = new CommentReplyViewModel
            {
                Author = authorName,
                CreationTime = DateTime.Now,
                IsApproved = false,
                Text = text,
                ParentCommentId = commentId
            };

            commentReply = _commentReplyApplicationService.AddCommentReply(commentReply);

            return RedirectToAction("Detail", "Products", new { ProductId = productId });
        }
    }
}
