﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using A5PWT_Eshop.Application.ApplicationServices.Orders;
using A5PWT_Eshop.Application.ApplicationServices.Security;
using A5PWT_Eshop.Domain.Constants;
using Microsoft.AspNetCore.Mvc;

namespace A5PWT_Eshop.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IOrderApplicationService _orderApplicationService;
        private readonly ISecurityApplicationService _securityApplicationService;

        public OrdersController(IOrderApplicationService orderApplicationService, ISecurityApplicationService securityApplicationService)
        {
            _orderApplicationService = orderApplicationService;
            _securityApplicationService = securityApplicationService;
        }
        public async Task<IActionResult> Create()
        {
            var user = await _securityApplicationService.GetCurrentUser(User);

            _orderApplicationService.CreateOrder(user.Id, Request.Cookies[Cookies.UserTrackingCode]);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Index()
        {
            var user = await _securityApplicationService.GetCurrentUser(User);
            var vm = _orderApplicationService.GetIndexViewModel(user.Id);
            return View(vm);
        }
    }
}